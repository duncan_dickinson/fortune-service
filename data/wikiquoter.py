#!/usr/bin/env python
import pprint
from operator import itemgetter

import requests

def get_tv_series(name):

    def name_checker(v):
        return v.startswith(name.replace('_', ' ')) and not v.endswith(' (disambiguation)')

    result = requests.get('http://en.wikiquote.org/w/api.php',
                          params={'format': 'json',
                                  'action': 'parse',
                                  'page': name.replace(' ', '_'),
                                  'prop': 'links'}).json()
    return filter(name_checker, list(map(itemgetter('*'), result['parse']['links'])))


def get_season_quotes(name):
    result = requests.get('http://en.wikiquote.org/w/api.php',
                          params={'format': 'json',
                                  'action': 'parse',
                                  'page': name.replace(' ', '_'),
                                  'prop': 'sections|text'}).json()
    page_title = result['parse']['title']
    page_links = result['parse']['sections']
    page_text = result['parse']['text']['*']
    return {'title': page_title,
            'links': page_links,
            'text': page_text}


series = get_tv_series('The Simpsons')

for season in series:
    print(season)
    season_quotes = get_season_quotes(season)

    from bs4 import BeautifulSoup
    soup = BeautifulSoup(season_quotes['text'], 'html.parser')

    for episode in soup.find_all('h3'):
        print('  - {}'.format(episode.i.string.encode('utf-8')))
