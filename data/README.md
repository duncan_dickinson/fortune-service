# wikiquote-fortune

URL: <https://github.com/maandree/wikiquote-fortune>

To grab a set of quotes from your favourite show:

````
docker run -it --rm --name py-script -v "$PWD":/usr/src/myapp -w /usr/src/myapp python:3.5.2 python wikiquote-fortune.py Seinfeld
````

Once downloaded, parse the file:

````
docker run -it --rm --name py-script -v "$PWD":/usr/src/myapp -w /usr/src/myapp python:3.5.2 python parse-quote.py
````

The resulting json files are located in the `data` directory and can be copied over to the `src/main/data` directory.
